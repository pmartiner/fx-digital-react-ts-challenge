import { merge } from 'webpack-merge';

// Plugins
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';

// Common
import {
  babelOptions,
  Configuration,
  webpackCommonConfig,
} from './webpack.common';

export const webpackConfig = (): Configuration => {
  return merge(webpackCommonConfig, {
    mode: 'production',
    devtool: 'source-map',
    target: 'web',
    module: {
      rules: [
        {
          test: /\.[jt]sx?$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: babelOptions,
            },
            {
              loader: 'ts-loader',
              options: {
                transpileOnly: true,
              },
            },
          ],
        },
        {
          test: /\.m?js$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: babelOptions,
            },
          ],
        },
      ],
    },
    plugins: [new ForkTsCheckerWebpackPlugin()],
  });
};

export default webpackConfig;
