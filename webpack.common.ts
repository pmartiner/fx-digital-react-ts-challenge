import path from 'path';
import { Configuration as WebpackConfiguration } from 'webpack';
import { Configuration as WebpackDevServerConfiguration } from 'webpack-dev-server';

// Plugins
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import { TsconfigPathsPlugin } from 'tsconfig-paths-webpack-plugin';
import ESLintPlugin from 'eslint-webpack-plugin';
import Dotenv from 'dotenv-webpack';

export interface Configuration extends WebpackConfiguration {
  devServer?: WebpackDevServerConfiguration;
}

export type EnvVars = {
  development?: boolean;
  production?: boolean;
};

export const isDevelopment = process.env.NODE_ENV !== 'production';

export const babelOptions = {
  presets: [
    '@babel/preset-env',
    '@babel/preset-react',
    [
      '@babel/preset-typescript',
      {
        allowDeclareFields: true,
      },
    ],
  ],
  plugins: [
    '@babel/plugin-transform-runtime',
    '@babel/proposal-object-rest-spread',
  ],
  sourceType: 'unambiguous',
};

export const webpackCommonConfig: Configuration = {
  entry: {
    main: path.resolve(__dirname, 'src', 'index.tsx'),
    vendors: ['react', 'react-dom'],
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    plugins: [new TsconfigPathsPlugin()],
  },
  /*
    I decided to split my bundle into chunks to improve the app's caching.
    Doing this will allow the browser to cache the heavy dependencies like react,
    while still cache-busting the bundled source code
    (`runtimeChunk: 'single' will help having only one react runtime instance`).
  */
  optimization: {
    moduleIds: 'deterministic',
    splitChunks: {
      chunks: 'all',
    },
    runtimeChunk: 'single',
  },
  output: {
    path: path.join(__dirname, 'build'),
    filename: '[name]-[contenthash].js',
    assetModuleFilename: 'images/[hash][ext][query]',
    clean: true,
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|gif)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.css$/i,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
            },
          },
          'postcss-loader',
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      hash: true,
      filename: 'index.html',
      template: './public/index.html',
      minify: {
        minifyCSS: true,
        collapseWhitespace: true,
        keepClosingSlash: true,
        removeComments: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
      },
    }),
    new ESLintPlugin({
      extensions: ['.tsx', '.ts', '.js', '.jsx'],
    }),
    new Dotenv({
      systemvars: true,
    }),
    new ForkTsCheckerWebpackPlugin(),
  ],
};
