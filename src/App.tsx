import { FC, useState, lazy, useEffect, Suspense } from 'react';
import { useQuery } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { Outlet } from 'react-router-dom';

// Styles
import './global.css';

// Types
import { CustomWindow } from '@/types/lib';

// API Fetchers
import getConfiguration from './api/get-configuration';

const ReactQueryDevtoolsProduction = lazy(() =>
  import('@tanstack/react-query-devtools/build/lib/index.prod.js').then(
    (d) => ({
      default: d.ReactQueryDevtools,
    })
  )
);

/**
 * The starting page for your App
 */

const App: FC = () => {
  const [showDevtools, setShowDevtools] = useState(false);
  const { isError, error } = useQuery(['configuration'], getConfiguration);

  useEffect(() => {
    (window as unknown as CustomWindow).toggleDevtools = () =>
      setShowDevtools((old) => !old);
  }, []);

  return (
    <>
      <>
        <section>
          <Outlet />
        </section>
        <div>
          {isError && (
            <p>
              Error while loading the API configuration:{' '}
              {(error as Error).message}
            </p>
          )}
        </div>
      </>
      <ReactQueryDevtools initialIsOpen={false} />
      {showDevtools && (
        <Suspense fallback={null}>
          <ReactQueryDevtoolsProduction />
        </Suspense>
      )}
    </>
  );
};

export default App;
