import { FC } from 'react';
import { useSearchParams } from 'react-router-dom';

// Pages
import ErrorPage from '@/pages/Error';

// API Fetchers
import SearchResults from '@/components/SearchResults';
const SearchPage: FC = () => {
  const [searchParams] = useSearchParams();
  const query = searchParams.get('q') || '';

  if (!searchParams.has('q')) {
    return <ErrorPage />;
  }

  return (
    <>
      <p className="p-3 text-lg text-indigo-900 lg:text-2xl">
        Your search results for:{' '}
        <span className="font-bold italic">{query}</span>
      </p>
      <SearchResults />
    </>
  );
};

export default SearchPage;
