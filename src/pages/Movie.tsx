import { useQuery } from '@tanstack/react-query';
import { FC } from 'react';
import { useParams } from 'react-router-dom';

// API Fetchers
import getConfiguration from '@/api/get-configuration';
import getMovieDetails from '@/api/get-movie-details';

// Components
import Spinner from '@/components/Spinner';

const MoviePage: FC = () => {
  const { movieId } = useParams();
  const {
    data: movieData,
    isLoading: isMovieLoading,
    isError: isMovieError,
    isSuccess: isMovieSuccess,
    error: movieError,
  } = useQuery(['movie', { movieId }], () =>
    getMovieDetails({
      movieId:
        movieId && !isNaN(parseInt(movieId, 10)) ? parseInt(movieId, 10) : -1,
    })
  );
  const {
    data: config,
    isLoading: isConfigLoading,
    isError: isConfigError,
    error: configError,
  } = useQuery(['configuration'], getConfiguration);

  const dollarFormat = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  });

  console.log(movieData && movieData.runtime! > 0);

  return (
    <>
      {(isMovieLoading || isConfigLoading) && (
        <div className="flex h-full items-center justify-center p-6">
          <Spinner />
        </div>
      )}
      {isMovieError && (
        <p className="mx-auto font-bold text-red-500">
          There was an error while loading the movie details:{' '}
          {(movieError as Error).message}
        </p>
      )}
      {isConfigError && (
        <p className="mx-auto font-bold text-red-500">
          Error while loading the API configuration:{' '}
          {(configError as Error).message}
        </p>
      )}
      {isMovieSuccess && (
        <>
          <div className="flex justify-center md:hidden">
            <h1 className="p-3 text-2xl font-bold text-indigo-900 lg:text-4xl">
              {movieData.title}{' '}
              {movieData.release_date && movieData.release_date !== '' && (
                <span className="font-normal">
                  ({movieData.release_date.substring(0, 4)})
                </span>
              )}
            </h1>
          </div>
          <div className="flex flex-col items-center gap-8 md:flex-row md:items-start">
            <div className="p-3">
              <img
                className="hidden rounded-lg md:block"
                src={`${
                  movieData.poster_path
                    ? `${config?.images?.secure_base_url}w500${movieData.poster_path}`
                    : 'https://via.placeholder.com/500x750?text=No+movie+poster'
                }`}
                alt={movieData.title}
              />
              <img
                className="rounded-lg md:hidden"
                src={`${
                  movieData.poster_path
                    ? `${config?.images?.secure_base_url}w185${movieData.poster_path}`
                    : 'https://via.placeholder.com/185x278?text=No+movie+poster'
                }`}
                alt={movieData.title}
              />
            </div>
            <div className="max-w-md md:max-w-lg lg:max-w-2xl">
              <div className="hidden justify-start md:flex">
                <h1 className="py-3 pb-5 text-2xl font-bold text-indigo-900 lg:text-4xl">
                  {movieData?.title}{' '}
                  {movieData.release_date && movieData.release_date !== '' && (
                    <span className="font-normal">
                      ({movieData.release_date.substring(0, 4)})
                    </span>
                  )}
                </h1>
              </div>
              {movieData.overview && (
                <p className="pb-4 text-indigo-900 lg:text-xl">
                  {movieData.overview} <span>({movieData.runtime} min)</span>
                </p>
              )}

              <div className="flex flex-row flex-wrap gap-6 md:gap-10">
                {movieData.budget > 0 && (
                  <p className="pb-4 text-indigo-900 lg:text-xl">
                    <span className="pb-2">Budget:</span> <br />
                    {dollarFormat.format(movieData.budget)}
                  </p>
                )}
                {movieData.revenue > 0 && (
                  <p className="pb-4 text-indigo-900 lg:text-xl">
                    <span className="pb-2">Gross Revenue:</span> <br />
                    {dollarFormat.format(movieData.revenue)}
                  </p>
                )}
              </div>
              {movieData.genres.length > 0 && (
                <>
                  <p className="pb-2 text-indigo-900 lg:text-xl">Genres:</p>
                  <ul className="flex flex-row flex-wrap pb-4">
                    {movieData.genres.map((g, i) => (
                      <li
                        key={g.id}
                        className={`m-2 rounded-lg bg-indigo-400 px-3 py-2 text-xs text-white ${
                          i === 0 ? 'ml-0' : ''
                        } ${i === movieData.genres.length - 1 ? 'mr-0' : ''}`}
                      >
                        {g.name}
                      </li>
                    ))}
                  </ul>
                </>
              )}
              <p className="pb-4 text-indigo-900 lg:text-xl">
                {movieData.title}{' '}
                {movieData.video ? (
                  <span>
                    <span className="font-bold">does</span> have a video.
                  </span>
                ) : (
                  <span>
                    <span className="font-bold">does not</span> have a video.
                  </span>
                )}
              </p>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default MoviePage;
