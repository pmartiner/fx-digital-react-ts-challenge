import { FC } from 'react';

// Components
import SearchForm from '@/components/SearchForm';
import Container from '@/components/Container';
import TrendingMovies from '@/components/TrendingMovies';

// Assets
import logo from '@/assets/img/fxdigitallogo.png';

const HomePage: FC = () => {
  return (
    <Container>
      <div className="my-20 mx-auto w-full max-w-3xl">
        <div className="my-10 flex flex-col items-center justify-center">
          <img src={logo} alt="FX Digital Logo" loading="eager" />
          <h1 className="mt-20 text-center text-3xl font-bold text-indigo-900 lg:text-4xl">
            Let&apos;s search for an amazing movie! 🎬
          </h1>
        </div>
        <SearchForm />
      </div>
      <div className="my-20 mx-auto w-full">
        <TrendingMovies />
      </div>
    </Container>
  );
};

export default HomePage;
