import { FC } from 'react';
import { isRouteErrorResponse, useRouteError } from 'react-router-dom';

const ErrorPage: FC = () => {
  const error = useRouteError() as Error;
  console.error(error);

  return (
    <div>
      <h1>Oops!</h1>
      <p>
        Sorry, an unexpected error has occurred. Are you in the right place?
      </p>
      <p>
        {isRouteErrorResponse(error) && (
          <div>
            <h2>{error.status}</h2>
            <p>{error.statusText}</p>
            {error.data?.message && <p>{error.data.message}</p>}
          </div>
        )}
      </p>
    </div>
  );
};

export default ErrorPage;
