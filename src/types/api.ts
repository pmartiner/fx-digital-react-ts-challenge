// Configuration types
export type TMDBConfigurationImages = {
  base_url?: string;
  secure_base_url?: string;
  backdrop_sizes?: string[];
  logo_sizes?: string[];
  poster_sizes?: string[];
  profile_sizes?: string[];
  still_sizes?: string[];
  change_keys?: string[];
};

export type TMDBConfiguration = {
  images?: TMDBConfigurationImages;
  change_keys?: string[];
};

// Movie details types
export type TMDBBelongsToCollection = {
  id: number;
  name: string;
  poster_path: string | null;
  backdrop_path: string;
};

export type TMDBGenre = {
  id: number;
  name: string;
};

export type TMDBProductionCompany = {
  id: number;
  logo_path: string | null;
  name: string;
  origin_country: string;
};

export type TMDBProductionCountry = {
  iso_3166_1: string;
  name: string;
};

export type TMDBSpokenLanguage = {
  english_name: string;
  iso_639_1: string;
  name: string;
};

export type TMDBMovieDetails = {
  adult: boolean;
  backdrop_path: string | null;
  belongs_to_collection: TMDBBelongsToCollection;
  budget: number;
  genres: TMDBGenre[];
  homepage: string | null;
  id: number;
  imdb_id: string | null;
  original_language: string;
  original_title: string;
  overview: string | null;
  popularity: number;
  poster_path: string | null;
  production_companies: TMDBProductionCompany[];
  production_countries: TMDBProductionCountry[];
  release_date?: string;
  revenue: number;
  runtime: number | null;
  spoken_languages: TMDBSpokenLanguage[];
  status: string;
  tagline: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
};

export type MovieDetailsRequest = {
  movieId: number;
  language?: string;
  appendToResponse?: string;
};

// Movie search types
export type TMDBMovieSearchResult = {
  adult: boolean;
  backdrop_path: string | null;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string | null;
  release_date?: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
};

export type TMDBMovieSearch = {
  page: number;
  results: TMDBMovieSearchResult[];
  total_results: number;
  total_pages: number;
};

export type MovieSearchRequest = {
  language?: string;
  query: string;
  page?: number;
  includeAdult?: boolean;
  region?: string;
  year?: number;
  primaryReleaseDate?: number;
};

// Trending movies types
export type TMDBTrendingMoviesWeek = TMDBMovieSearch;
