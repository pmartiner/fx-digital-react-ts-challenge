import { ReactNode } from 'react';

// Types
import { TMDBMovieSearch, TMDBMovieSearchResult } from '@/types/api';

export type CardProps = {
  image: string;
  title: string;
  isFirst?: boolean;
  isLast?: boolean;
  movieId: number;
  shouldLazyLoadImage?: boolean;
};

export type SearchFormProps = {
  initialValue?: string;
};

export type ChildrenProps = {
  children?: ReactNode;
};

export type FilterCollapsibleProps = {
  setFilteredData: (data: TMDBMovieSearchResult[]) => void;
  searchData: TMDBMovieSearch;
  isSearchLoading: boolean;
  isSearchError: boolean;
  isSearchSuccess: boolean;
  searchError: unknown;
};
