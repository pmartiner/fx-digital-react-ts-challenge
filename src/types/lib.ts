export type CustomWindow = Window & {
  toggleDevtools: () => void;
};
