import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

// Compontents
import App from '@/App';
import Layout from '@/components/Layout';

// Pages
import HomePage from '@/pages/Home';
import SearchPage from '@/pages/Search';
import ErrorPage from '@/pages/Error';
import MoviePage from '@/pages/Movie';

const container = document.getElementById('root');
const root = createRoot(container!);
const queryClient = new QueryClient();

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: '/search',
        element: (
          <Layout>
            <SearchPage />
          </Layout>
        ),
        errorElement: <ErrorPage />,
      },
      {
        path: '/movie/:movieId',
        element: (
          <Layout>
            <MoviePage />
          </Layout>
        ),
        errorElement: <ErrorPage />,
      },
    ],
    errorElement: <ErrorPage />,
  },
]);

root.render(
  <StrictMode>
    <QueryClientProvider client={queryClient}>
      <RouterProvider router={router} />
    </QueryClientProvider>
  </StrictMode>
);
