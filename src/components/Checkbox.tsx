import { FC, InputHTMLAttributes } from 'react';

const Checkbox: FC<InputHTMLAttributes<HTMLInputElement>> = (props) => (
  <input
    {...props}
    type="checkbox"
    className="border-3 h-6 w-6
      cursor-pointer rounded-lg
      border-0 bg-gray-200
      checked:bg-indigo-300 hover:bg-gray-300 hover:checked:bg-indigo-400
      focus:outline-none focus:ring-4 focus:ring-violet-300/75 focus:checked:bg-indigo-300"
  />
);

export default Checkbox;
