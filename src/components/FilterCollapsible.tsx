import { FC, useEffect, useState } from 'react';
/*
  Even though HeadlessUI and React Icons will make the build heavier,
  I decided to add this libraries because they're tree-shakable, they improve the user experience
  (accessibilty without reinventing the wheel with the former and iconography with the latter)
  and they'll help saving some time given the 72 hours constraint for the project submission
*/
import { Disclosure, Transition } from '@headlessui/react';
import { BsFilter } from 'react-icons/bs';

// Components
import Spinner from '@/components/Spinner';
import Select from '@/components/Select';
import Checkbox from '@/components/Checkbox';

// Types
import { FilterCollapsibleProps } from '@/types/props';

const FilterCollapsible: FC<FilterCollapsibleProps> = ({
  searchData,
  isSearchLoading,
  isSearchError,
  isSearchSuccess,
  searchError,
  setFilteredData,
}) => {
  const [yearOptions, setYearOptions] = useState<string[]>([]);
  const [selectedYear, setSelectedYear] = useState<string | 'all'>('all');
  const [hasVideo, setHasVideo] = useState<boolean>();

  useEffect(() => {
    const movieYears = searchData?.results.map((movie) =>
      (movie.release_date || '').substring(0, 4)
    );

    const yearSet = new Set(movieYears);
    const uniqueYears = Array.from(yearSet)
      .filter((el) => el !== '')
      .sort((a, b) => parseInt(a, 10) - parseInt(b, 10));

    setYearOptions(uniqueYears);
  }, [searchData]);

  useEffect(() => {
    const filteredMoviesByYear =
      selectedYear === 'all'
        ? searchData?.results || []
        : (searchData?.results || []).filter(
            (movie) =>
              (movie.release_date || '').substring(0, 4) === selectedYear
          );

    const filteredMoviesWithVideo =
      typeof hasVideo === 'undefined'
        ? filteredMoviesByYear
        : filteredMoviesByYear.filter((movie) => movie.video === hasVideo);

    setFilteredData(filteredMoviesWithVideo);
  }, [selectedYear, hasVideo, searchData]);

  return (
    <Disclosure>
      <Disclosure.Button className="mx-auto flex items-center gap-2 rounded-xl bg-gray-200 p-3 text-lg text-indigo-900 focus:outline-none focus:ring-4 focus:ring-violet-300/75 lg:text-2xl">
        <BsFilter />
        Filter
      </Disclosure.Button>
      <Transition
        enter="origin-top transition duration-200 ease-in"
        enterFrom="transform scale-y-95 opacity-0"
        enterTo="transform scale-y-100 opacity-100"
        leave="origin-top transition duration-200 ease-out"
        leaveFrom="transform scale-y-100 opacity-100"
        leaveTo="transform scale-y-95 opacity-0"
      >
        <Disclosure.Panel className="pt-10 pb-4">
          {isSearchLoading && (
            <div className="flex h-full items-center justify-center p-6">
              <Spinner />
            </div>
          )}
          {isSearchError && (
            <p className="mx-auto font-bold text-red-500">
              There was an error while loading your search results:{' '}
              {(searchError as Error).message}
            </p>
          )}
          {isSearchSuccess && (
            <form>
              <div className="flex w-full flex-col flex-wrap items-center justify-center gap-5 sm:flex-row">
                <div className="max-w-[220px]">
                  <label
                    className="text-indigo-900 lg:text-lg"
                    htmlFor="release-year"
                  >
                    Release year:
                  </label>
                  <Select
                    id="release-year"
                    aria-label="Select a release year to filter the movies"
                    onChange={(e) => setSelectedYear(e.target.value)}
                    value={selectedYear}
                  >
                    <option disabled>Select a release year</option>
                    <option value="all">All</option>
                    {yearOptions.map((y) => (
                      <option key={y} value={y}>
                        {y}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="flex items-end sm:pt-4">
                  <label
                    htmlFor="with-video"
                    className="mr-3 text-indigo-900 sm:mx-3 lg:text-lg"
                  >
                    With video?
                  </label>
                  <Checkbox
                    onChange={(e) => setHasVideo(e.target.checked)}
                    checked={hasVideo}
                    name="with-video"
                    id="with-video"
                  />
                </div>
              </div>
            </form>
          )}
        </Disclosure.Panel>
      </Transition>
    </Disclosure>
  );
};

export default FilterCollapsible;
