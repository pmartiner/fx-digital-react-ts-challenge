import { FC } from 'react';
import { Link } from 'react-router-dom';

// Types
import { CardProps } from '@/types/props';

// Components
import DarkenedBackground from '@/components/DarkenedBackground';

const MovieCard: FC<CardProps> = (props) => {
  const { image, title, movieId, shouldLazyLoadImage } = props;
  return (
    <Link
      className="w-100 h-100 group focus:outline-none"
      to={`/movie/${movieId}`}
    >
      <div
        className={
          'relative flex w-28 flex-col items-center justify-center rounded-lg ring-offset-1 focus:outline-none group-focus:outline-none group-focus:ring-4 group-focus:ring-violet-300/75 sm:w-32 lg:w-56'
        }
      >
        <img
          loading={shouldLazyLoadImage ? 'lazy' : undefined}
          alt={`${title} poster`}
          className="w-48 rounded-lg lg:w-56"
          src={image}
        />
        <DarkenedBackground>
          <p className="mt-auto p-4 font-bold text-white">{title}</p>
        </DarkenedBackground>
        <p className="sr-only">{title}</p>
      </div>
    </Link>
  );
};

export default MovieCard;
