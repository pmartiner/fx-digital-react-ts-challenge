import { FC, SelectHTMLAttributes } from 'react';

const Select: FC<SelectHTMLAttributes<HTMLSelectElement>> = ({
  children,
  ...rest
}) => {
  return (
    <select
      className="m-0
        block
        w-full
        appearance-none
        rounded-lg
        border-2
        border-solid
        border-indigo-300
        bg-white
        bg-select-arrow bg-[length:16px_12px] bg-clip-padding
        bg-[right_.75rem_center]
        bg-no-repeat p-3 text-base
        font-normal
        text-indigo-900
        transition
        ease-in-out
        focus:border-indigo-300 focus:outline-none focus:ring-0
        focus-visible:outline-none focus-visible:ring-4 focus-visible:ring-violet-300/75"
      {...rest}
    >
      {children}
    </select>
  );
};

export default Select;
