import { FC } from 'react';

// Components
import Header from '@/components/Header';
import Container from '@/components/Container';

// Types
import { ChildrenProps } from '@/types/props';

const Layout: FC<ChildrenProps> = ({ children }) => (
  <>
    <Header />
    <main>
      <Container>{children}</Container>
    </main>
  </>
);

export default Layout;
