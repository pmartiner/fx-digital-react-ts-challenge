import { FC } from 'react';
import { useQuery } from '@tanstack/react-query';

// Components
import MovieCard from '@/components/MovieCard';
import Spinner from '@/components/Spinner';

// API Fetcher
import getTrendingMoviesWeek from '@/api/get-trending-movies-week';
import getConfiguration from '@/api/get-configuration';

const TrendingMovies: FC = () => {
  const {
    isLoading: isTrendingLoading,
    isError: isTrendingError,
    data: trendingData,
    error: trendingError,
  } = useQuery(['trending-movies'], getTrendingMoviesWeek);
  const {
    data: config,
    isSuccess: isConfigSuccess,
    isLoading: isConfigLoading,
    isError: isConfigError,
    error: configError,
  } = useQuery(['configuration'], getConfiguration);

  return (
    <div className="my-20 mx-auto w-full">
      <h2 className="py-4 text-center text-2xl font-bold text-violet-900 lg:text-3xl">
        Movies trending this week:
      </h2>

      {(isTrendingLoading || isConfigLoading) && (
        <div className="flex h-[288px] justify-center p-6 lg:h-[336px]">
          <Spinner />
        </div>
      )}
      {isConfigError && (
        <p className="mx-auto font-bold text-red-500">
          Error while loading the API configuration:{' '}
          {(configError as Error).message}
        </p>
      )}
      {isTrendingError && (
        <p className="mx-auto font-bold text-red-500">
          Error while loading the movies: {(trendingError as Error).message}
        </p>
      )}
      {isConfigSuccess && isConfigSuccess && (
        <ul className="flex max-w-7xl flex-row items-start overflow-auto p-3 ring-offset-2 focus:rounded-lg focus:outline-none focus:ring-4 focus:ring-violet-300/75">
          {trendingData?.results.slice(0, 10).map((movie, i) => (
            <li
              className={`${i === 0 ? ' ml-0 ' : ''}${
                i === trendingData?.results.length - 1 ? ' mr-0 ' : ''
              } m-3 lg:m-6`}
              key={movie.id}
            >
              <MovieCard
                shouldLazyLoadImage
                movieId={movie.id}
                image={`${
                  movie.poster_path
                    ? `${config?.images?.secure_base_url}w342${movie.poster_path}`
                    : 'https://via.placeholder.com/224x336?text=No+movie+poster'
                }`}
                title={movie.title}
              />
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default TrendingMovies;
