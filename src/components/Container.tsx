import { FC } from 'react';

// Types
import { ChildrenProps } from '@/types/props';

const Container: FC<ChildrenProps> = ({ children }) => (
  <div className="mx-auto w-full max-w-[1400px] px-4">{children}</div>
);

export default Container;
