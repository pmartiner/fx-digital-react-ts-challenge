import { Link, useSearchParams } from 'react-router-dom';
import { FC } from 'react';

// Assets
import logo from '@/assets/img/fxdigitallogo.png';
import smallLogo from '@/assets/img/cropped-FX-Favicon-270x270.png';

// Components
import Container from '@/components/Container';
import SearchForm from '@/components/SearchForm';

const Header: FC = () => {
  const [searchParams] = useSearchParams();
  const query = searchParams.get('q') || undefined;

  return (
    <header>
      <nav>
        <div className="py-6">
          <Container>
            <div className="flex flex-row items-center gap-5">
              <Link
                className="hidden rounded-lg p-4 focus:outline-none focus:ring-4 focus:ring-violet-300/75 lg:block"
                aria-label="Go to Home"
                to="/"
              >
                <img src={logo} alt="FX Digital Logo" loading="eager" />
              </Link>
              <Link
                className="block max-w-[60px] rounded-lg p-1 focus:outline-none focus:ring-4 focus:ring-violet-300/75 lg:hidden"
                aria-label="Go to Home"
                to="/"
              >
                <img src={smallLogo} alt="FX Digital Logo" loading="eager" />
              </Link>
              <SearchForm initialValue={query} />
            </div>
          </Container>
        </div>
      </nav>
    </header>
  );
};

export default Header;
