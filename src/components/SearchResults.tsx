import { FC, useEffect, useState } from 'react';
import getMovieSearchResults from '@/api/search-movies';
import { useSearchParams, Navigate } from 'react-router-dom';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { BiChevronRight, BiChevronLeft } from 'react-icons/bi';

// Components
import Spinner from '@/components/Spinner';
import MovieCard from '@/components/MovieCard';
import FilterCollapsible from '@/components/FilterCollapsible';

// API Fetchers
import getConfiguration from '@/api/get-configuration';

// Types
import { TMDBMovieSearchResult } from '@/types/api';

/*
  Given that TMDB has a search API, I decided to use it instead of creating a local-based search feature.
  The reasoning behind this was to create a more powerful app. However, assuming that
  I would've developed a client-based search component, I would have been a controlled component.
  But, to make the search filter more performant, I would've throttled the filtering, so that the
  process would begin a few seconds after the user typed their input.
*/
const SearchResults: FC = () => {
  const [searchParams] = useSearchParams();
  const queryClient = useQueryClient();
  const query = searchParams.get('q') || '';
  const page =
    searchParams.get('page') !== null
      ? parseInt(searchParams.get('page')!, 10)
      : 1;
  const queryParams = new URLSearchParams({
    q: query,
  });
  const {
    data: searchData,
    isLoading: isSearchLoading,
    isError: isSearchError,
    isSuccess: isSearchSuccess,
    error: searchError,
  } = useQuery(
    ['search', { query, page }],
    () => getMovieSearchResults({ query, page }),
    { keepPreviousData: true }
  );
  const {
    data: config,
    isLoading: isConfigLoading,
    isError: isConfigError,
    error: configError,
  } = useQuery(['configuration'], getConfiguration);

  const [filteredData, setFilteredData] = useState<TMDBMovieSearchResult[]>(
    searchData?.results || []
  );

  useEffect(() => {
    if (searchData?.total_pages || 0 <= page) {
      queryClient.prefetchQuery(['search', { query, page }], () =>
        getMovieSearchResults({ query, page })
      );
    }
  }, [searchData, page, queryClient]);

  if (query === '') {
    return <Navigate to="/" replace={true} />;
  }

  return (
    <div>
      {(isSearchLoading || isConfigLoading) && (
        <div className="flex h-full items-center justify-center p-6">
          <Spinner />
        </div>
      )}
      {isSearchError && (
        <p className="mx-auto font-bold text-red-500">
          There was an error while loading your search results:{' '}
          {(searchError as Error).message}
        </p>
      )}
      {isConfigError && (
        <p className="mx-auto font-bold text-red-500">
          Error while loading the API configuration:{' '}
          {(configError as Error).message}
        </p>
      )}
      {isSearchSuccess && (
        <>
          <div className="pb-8">
            <FilterCollapsible
              setFilteredData={setFilteredData}
              searchData={searchData}
              isSearchLoading={isSearchLoading}
              isSearchError={isSearchError}
              isSearchSuccess={isSearchSuccess}
              searchError={searchError}
            />
          </div>
          <section>
            {filteredData.length === 0 && (
              <div className="mt-10 flex w-full justify-center">
                <p className="text-lg font-bold text-indigo-900 lg:text-2xl">
                  {' '}
                  No results were found.
                </p>
              </div>
            )}
            {filteredData.length > 0 && (
              <ul className="flex flex-row flex-wrap justify-center pb-10">
                {filteredData.map((movie) => (
                  <li className="lg:m-6' m-3" key={movie.id}>
                    <MovieCard
                      movieId={movie.id}
                      image={`${
                        movie.poster_path
                          ? `${config?.images?.secure_base_url}w342${movie.poster_path}`
                          : 'https://via.placeholder.com/224x336?text=No+movie+poster'
                      }`}
                      title={movie.title}
                    />
                  </li>
                ))}
              </ul>
            )}
            <div className="mb-10 flex flex-row items-center justify-center gap-5">
              <a
                className={`flex items-center gap-1 rounded-lg p-4 text-lg font-bold text-indigo-900 visited:text-violet-700 focus:outline-none focus:ring-4 focus:ring-violet-300/75 lg:text-2xl ${
                  page === 1
                    ? 'pointer-events-none cursor-not-allowed text-gray-500'
                    : 'decoration-2 hover:underline'
                }`}
                href={`/search?${queryParams.toString()}${
                  page - 1 === 1 ? '' : `&page=${page - 1}`
                }`}
              >
                <BiChevronLeft />
                Anterior
              </a>
              <a
                className={`flex items-center gap-1 rounded-lg p-4 text-lg font-bold text-indigo-900 visited:text-violet-700 focus:outline-none focus:ring-4 focus:ring-violet-300/75 lg:text-2xl ${
                  page === searchData.total_pages
                    ? 'pointer-events-none cursor-not-allowed text-gray-500'
                    : 'decoration-2 hover:underline'
                }`}
                href={`/search?${queryParams.toString()}&page=${page + 1}`}
              >
                Siguiente <BiChevronRight />
              </a>
            </div>
          </section>
        </>
      )}
    </div>
  );
};

export default SearchResults;
