import { FC } from 'react';

// Types
import { ChildrenProps } from '@/types/props';

const DarkenedBackground: FC<ChildrenProps> = ({ children }) => (
  <div className="absolute hidden h-full w-full rounded-lg bg-black/70 group-hover:flex">
    {children}
  </div>
);

export default DarkenedBackground;
