import { FC } from 'react';
import { Form } from 'react-router-dom';
import { BiSearch } from 'react-icons/bi';

// Types
import { SearchFormProps } from '@/types/props';

const SearchForm: FC<SearchFormProps> = ({ initialValue }) => (
  <Form role="search" className="group w-full" action="/search">
    <div className="flex flex-row rounded-2xl hover:shadow-2xl">
      <input
        id="search"
        className="w-full rounded-2xl rounded-r-none border-2 border-r-0 border-indigo-300 p-4 text-base text-indigo-900 transition focus:border-indigo-300 focus:outline-none focus:ring-0"
        aria-label="Search movies"
        placeholder="Search for a movie..."
        type="search"
        name="q"
        defaultValue={initialValue}
      />
      <div id="search-spinner" aria-hidden hidden={true} />
      <div className="sr-only" aria-live="polite"></div>
      <button
        className="rounded-2xl rounded-l-none border-2 border-l-0 border-indigo-300 p-4 text-2xl text-indigo-900 transition hover:border-l-2 hover:border-indigo-900 hover:bg-indigo-900 hover:text-white focus:border-indigo-900 focus:bg-indigo-900 focus:text-white focus:outline-none"
        type="submit"
        aria-label="Search"
      >
        <BiSearch />
      </button>
    </div>
  </Form>
);

export default SearchForm;
