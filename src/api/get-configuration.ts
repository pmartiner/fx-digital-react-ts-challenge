// Types
import { TMDBConfiguration } from '@/types/api';

const params = new URLSearchParams({
  api_key: process.env.TMDB_API_KEY || '',
});
const url = `https://api.themoviedb.org/3/configuration?${params.toString()}`;

const getConfiguration = async () => {
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`Error ${response.status}: ${response.statusText}`);
  }
  return response.json() as Promise<TMDBConfiguration>;
};

export default getConfiguration;
