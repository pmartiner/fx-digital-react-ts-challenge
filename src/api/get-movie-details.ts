// Types
import { MovieDetailsRequest, TMDBMovieDetails } from '@/types/api';

const getMovieDetails = async ({
  movieId,
  language,
  appendToResponse,
}: MovieDetailsRequest) => {
  const params = new URLSearchParams({
    api_key: process.env.TMDB_API_KEY || '',
  });

  if (language) {
    params.append('language', language.toString());
  }

  if (appendToResponse) {
    params.append('append_to_response', appendToResponse.toString());
  }

  const url = `https://api.themoviedb.org/3/movie/${movieId}?${params.toString()}`;

  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`Error ${response.status}: ${response.statusText}`);
  }
  return response.json() as Promise<TMDBMovieDetails>;
};

export default getMovieDetails;
