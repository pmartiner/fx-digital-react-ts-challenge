// Types
import { TMDBTrendingMoviesWeek } from '@/types/api';

const params = new URLSearchParams({
  api_key: process.env.TMDB_API_KEY || '',
});
const url = `https://api.themoviedb.org/3/trending/movie/week?${params.toString()}`;

const getTrendingMoviesWeek = async () => {
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`Error ${response.status}: ${response.statusText}`);
  }
  return response.json() as Promise<TMDBTrendingMoviesWeek>;
};

export default getTrendingMoviesWeek;
