// Types
import { MovieSearchRequest, TMDBMovieSearch } from '@/types/api';

const getMovieSearchResults = async ({
  query,
  page,
  includeAdult,
  year,
  primaryReleaseDate,
  language,
  region,
}: MovieSearchRequest) => {
  const params = new URLSearchParams({
    api_key: process.env.TMDB_API_KEY || '',
    query,
  });

  if (page) {
    params.append('page', page.toString());
  }

  if (includeAdult) {
    params.append('include_adult', includeAdult.toString());
  }

  if (year) {
    params.append('year', year.toString());
  }

  if (primaryReleaseDate) {
    params.append('primary_release_year', primaryReleaseDate.toString());
  }

  if (region) {
    params.append('region', region.toString());
  }

  if (language) {
    params.append('language', language.toString());
  }

  const url = `https://api.themoviedb.org/3/search/movie?${params.toString()}`;
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`Error ${response.status}: ${response.statusText}`);
  }
  return response.json() as Promise<TMDBMovieSearch>;
};

export default getMovieSearchResults;
