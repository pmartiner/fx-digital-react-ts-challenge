module.exports = {
  "semi": true,
  "trailingComma": "es5",
  "jsxSingleQuote": false,
  "singleQuote": true,
  "printWidth": 80,
  "tabWidth": 2,
  "bracketSameLine": false,
  plugins: [require('prettier-plugin-tailwindcss')],
}
